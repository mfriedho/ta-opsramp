
# encoding = utf-8

import os
import sys
import time
import time
import math
import json
import re

'''
    IMPORTANT
    Edit only the validate_input and collect_events functions.
    Do not edit any other part in this file.
    This file is generated only once when creating the modular input.
'''
'''
# For advanced users, if you want to create single instance mod input, uncomment this method.
def use_single_instance_mode():
    return True
'''

def validate_input(helper, definition):
    """
    proxy = helper.proxy
    helper.log_info("Proxy info: %s" % (proxy)) 
    
    baseurl = definition.parameters.get('api_endpoint')
    partner_tenant_id = definition.parameters.get('partner_tenant_id')
    partner_api_key = definition.parameters.get('partner_api_key')
    partner_api_secret = definition.parameters.get('partner_api_secret')
    inventory_refresh_frequency = definition.parameters.get('inventory_refresh_frequency')
    stanza = helper.get_input_stanza()
     
    helper.log_info("Using proxy: %s" % (has_proxy_configured(helper)))  
   
    bearer_token = get_session_token(baseurl, partner_api_key, partner_api_secret, helper)
    helper.log_debug('Obtained bearer_token: %s' % (bearer_token))
    
    if bearer_token is None:
        raise ValueError("Cannot connect to %s with provided Partner API Key and Partner API Secret!  Please check values including proxy settings." % (baseurl))
        
    """
    pass
   


    """
    interval = None
    if 'interval' in stanza.values()[0]:
        interval = stanza.values()[0]['interval']
    pass
    """

def collect_events(helper, ew):
    global api_stats
    api_stats = (9999,9999,9999)
    
    helper.log_info('Starting OpsRamp data collection')
    
    #
    # Setup & get input config parameters
    #
    
    baseurl = helper.get_arg('api_endpoint')
    partner_tenant_id = helper.get_arg('partner_tenant_id')
    partner_api_key = helper.get_arg('partner_api_key')
    partner_api_secret = helper.get_arg('partner_api_secret')
    inventory_refresh_frequency = helper.get_arg('inventory_refresh_frequency')
    resource_refresh_interval = helper.get_arg('resource_refresh_interval')
    inputname = helper.get_input_stanza_names()
    
    helper.log_info('Input props baseurl:%s, partner_tenant_id:%s, inventory_refresh_frequency:%s' % (baseurl,partner_tenant_id,inventory_refresh_frequency))
    helper.log_debug('Using OAuth2 creds partner_api_key:%s, partner_api_secret:%s' % (partner_api_key,partner_api_secret))
    
    #
    # Get oauth2 bearer token using key and secret
    #
    bearer_token = get_session_token(baseurl, partner_api_key, partner_api_secret, helper)
    helper.log_debug('Obtained bearer_token: %s' % (bearer_token))

    
    #
    # Get tenant_resource_metrics from checkpoint state or rebuild them if they are in need of refreshing
    #
    resource_refresh_interval = int(inventory_refresh_frequency)*60 # Minutes to seconds
    tenant_resource_metrics = get_tenant_resource_metrics(baseurl, bearer_token, partner_tenant_id, resource_refresh_interval, helper, ew)

    #
    # Get resources_last_data timstamp state
    #
    resources_last_data = get_resources_last_data(helper)
    helper.log_info("Starting resources_last_data: %s" % (json.dumps(resources_last_data)))
 
    #
    # Iterate over tenant resource metrics in order of oldest to newest lastdata to pull the actual time series data points
    #
    sort_by_lastdata = []
    for tenant in tenant_resource_metrics.keys():
        if tenant not in resources_last_data.keys():
            resources_last_data[tenant] = {}
        for resource in tenant_resource_metrics[tenant].keys():
            # Timestamp of most recent seen data point for this resource
            if resource not in resources_last_data[tenant].keys():
                now = math.trunc(time.time())
                resources_last_data[tenant][resource] = now - resource_refresh_interval
            lastdata = resources_last_data[tenant][resource]
            sort_by_lastdata.append((tenant,resource,lastdata))
    sort_by_lastdata.sort(key=lambda x: x[2])
    for (tenant, resource, lastdata) in sort_by_lastdata:
        startTime = lastdata
        endTime = math.trunc(time.time())
        metrics = tenant_resource_metrics[tenant][resource]
        api_calls_needed = len(metrics)
        (api_calls_rate_limit, api_calls_remaining, api_limit_reset_time) = api_stats
        if api_calls_needed > api_calls_remaining:
            helper.log_warning('OpsRamp API throttling limit reached.  api_calls_rate_limit: %i, api_calls_remaining: %i, api_limit_reset_time: %i - Will try to catch up on next input run.  If we never catch up and data is getting behind for some resources consider running the input more frequently, up to once per minute.' % (api_calls_rate_limit, api_calls_remaining, api_limit_reset_time))
            break
        for metric in metrics:
            #
            # Get time series values for each tenant resource metric
            #
            metricdatasets = get_metricdatasets(baseurl, bearer_token, tenant, resource, metric, startTime, endTime, helper)
            
            for dset in metricdatasets:
                for tstamp in dset['data']:
                    try:
                        dpoint = {}
                        dpoint['value'] = dset['data'][tstamp]
                        dpoint['tenant_uniqueId'] = tenant
                        dpoint['resource_id'] = resource
                        dpoint['component'] = dset['component']
                        dpoint['metricName'] = metric
                        dpoint['timestamp'] = tstamp
                        ew.write_event(helper.new_event(source=baseurl, index=helper.get_output_index(), sourcetype=helper.get_sourcetype()+':metricdata', data=json.dumps(dpoint)))
                        if int(tstamp) >= lastdata:
                            lastdata = int(tstamp)+1
                    except Exception as e:
                        helper.log_warning("Exception occurred: %s" % (e))
                        
        resources_last_data[tenant][resource] = lastdata
    helper.save_check_point('resources_last_data', resources_last_data)
    #helper.log_info('get_metricdataset: Headers returned: %s.' % (r_headers))
    
    #checkpoint = max(checkpoint, item["content"]["timestamp"])
   
    
    #checkpoint[tenant['uniqueId']][resource['id']]['lastdata'] = lastdata    
  
    helper.log_debug("Ending tenant_resource_metrics: %s" % (json.dumps(tenant_resource_metrics)))
    helper.log_debug("Ending resources_last_data: %s" % (json.dumps(resources_last_data)))
    helper.log_debug("Input Stanza: %s" % (json.dumps(helper.get_input_stanza(inputname))))
    helper.log_debug("Proxy: %s" % (json.dumps(helper.get_proxy())))
    
    #helper.save_check_point('resource_metrics', checkpoint)
    #helper.save_check_point(inputname, checkpoint)
    #helper.log_info('New checkpoint for %s saved: %s' % (inputname,checkpoint))        
    



###############################################################################

def has_proxy_configured(helper):
    try:
        proxy = helper.get_proxy()
        if 'proxy_url' in proxy:
            if proxy['proxy_url'] is not None:
                return True
    except Exception as e:
        return False
    return False

        
def update_api_stats(response, helper):
    global api_stats
    r_headers = response.headers
    api_calls_rate_limit = int(r_headers['x-ratelimit-limit'])
    api_calls_remaining = int(r_headers['x-ratelimit-remaining'])
    api_limit_reset_time = int(r_headers['x-ratelimit-reset'])
    api_stats = (api_calls_rate_limit, api_calls_remaining, api_limit_reset_time)
    helper.log_debug('OpsRamp API Rate Stats - api_calls_rate_limit: %i, api_calls_remaining: %i, api_limit_reset_time: %i' % (api_calls_rate_limit, api_calls_remaining, api_limit_reset_time)) 
    
    
def cleanup_resources_last_data(tenant_resource_metrics, resource_refresh_interval, helper):
    resources_last_data = get_resources_last_data(helper)
    cleaned_resources_last_data = {}
    
    for tenant in tenant_resource_metrics.keys():
        if tenant not in resources_last_data.keys():
            resources_last_data[tenant] = {}
        cleaned_resources_last_data[tenant] = {}
        for resource in tenant_resource_metrics[tenant].keys():
            # Timestamp of most recent seen data point for this resource
            if resource not in resources_last_data[tenant].keys():
                now = math.trunc(time.time())
                resources_last_data[tenant][resource] = now - resource_refresh_interval

            now = math.trunc(time.time())
            age_in_sec = now - resources_last_data[tenant][resource]
            if (age_in_sec) > resource_refresh_interval:
                resources_last_data[tenant][resource] = now - resource_refresh_interval
            cleaned_resources_last_data[tenant][resource] = resources_last_data[tenant][resource]
            
    helper.save_check_point('resources_last_data', cleaned_resources_last_data)
    helper.log_info("Cleaned up resources_last_data")
            

def get_resources_last_data(helper):
    resources_last_data = helper.get_check_point('resources_last_data')
    if resources_last_data is None:
        resources_last_data = {}
    return resources_last_data


def get_resource_details(baseurl, bearer_token, tenant, resource, helper):
    url = baseurl + '/api/v2/tenants/' + tenant + '/resources/' + resource
    headers = {
        'Content-Type'      : 'application/json',
        'Accept'            : 'application/json',
        'Authorization'     : 'Bearer ' + bearer_token
    }
    method = "GET"
    response = helper.send_http_request(
        url,
        method, 
        parameters=None, 
        headers=headers, 
        verify=False, 
        timeout=None, 
        use_proxy=has_proxy_configured(helper)
    )
    update_api_stats(response, helper)
    resource_details = json.loads(response.text)
    return resource_details    
        
def get_tenant_resource_metrics(baseurl, bearer_token, partner_tenant_id, resource_refresh_interval, helper, ew):
    tenant_resource_metrics = {}
    resources_need_refreshing = False
    resources_last_refreshed = helper.get_check_point('resources_last_refreshed')
    if resources_last_refreshed is None:
        resources_last_refreshed = 0
    now = math.trunc(time.time())
    age_in_sec = now - resources_last_refreshed
    helper.log_info('Resources were last refreshed %i seconds ago' % (age_in_sec))
    if (age_in_sec) > resource_refresh_interval:
        resources_need_refreshing = True
        helper.log_info('Resources need to be refreshed')
        tenant_resource_metrics = refresh_tenant_resource_metrics(baseurl, bearer_token, partner_tenant_id, helper, ew)
        helper.save_check_point('tenant_resource_metrics', tenant_resource_metrics)
        helper.save_check_point('resources_last_refreshed', now)
        helper.log_info('Saved new tenant_resource_metrics checkpoint')
        #
        # Need to remove resources that are no longer in inventory from resources_last_data checkpoint so that it doesn't bloat over time
        #
        cleanup_resources_last_data(tenant_resource_metrics, resource_refresh_interval, helper)   
        
    else:
        tenant_resource_metrics = helper.get_check_point('tenant_resource_metrics')
        helper.log_info('Loaded existing tenant_resource_metrics checkpoint')
        
    helper.log_debug('tenant_resource_metrics: %s' % (tenant_resource_metrics))
    return tenant_resource_metrics
    

def refresh_tenant_resource_metrics(baseurl, bearer_token, partner_tenant_id, helper, ew):
    tenant_resource_metrics = {}
    #
    # Get list of clients/tenants to iterate over
    #
    tenants = get_tenants(baseurl, bearer_token, partner_tenant_id, helper, ew)
    
    #
    # Iterate over tenants
    #
    for tenant in tenants:
        tenant['tenant_uniqueId'] = tenant['uniqueId']
        
        #
        # Create tenant event for each tenant
        #
        ew.write_event(helper.new_event(source=baseurl, index=helper.get_output_index(), sourcetype=helper.get_sourcetype()+':tenant', data=json.dumps(tenant)))
        tenant_resource_metrics[tenant['uniqueId']] = {}
        
        #
        # Get resources for each tenant
        #
    
        resources = get_resources(baseurl, bearer_token, tenant, helper)
        
        #
        # Iterate over this tenant's resources
        #
        for resource in resources:
            lastdata = 0
            resource['tenant_id'] = tenant['id']
            resource['tenant_uniqueId'] = tenant['uniqueId']
            resource['tenant_name'] = tenant['name']
            resource['details'] = get_resource_details(baseurl, bearer_token, tenant['uniqueId'], resource['id'], helper)
            

            #
            # Remove metricTypes and installedApp as they are too verbose
            # Note: later after creating netword card detail records we delete that
            # too as it is also too verbose
            #
            if "metricTypes" in resource['details']:
                del resource['details']['metricTypes']
            if "installedApp" in resource['details']:
                del resource['details']['installedApp']

            
            #
            # If there are networkCardDetails on the resource, create the records of that sourcetype too
            #
            if "networkCardDetails" in resource['details']:
                for card in resource['details']['networkCardDetails']:
                    card['tenant_id'] = tenant['id']
                    card['tenant_uniqueId'] = tenant['uniqueId']
                    card['tenant_name'] = tenant['name']
                    card['resource_id'] = resource['id']
                    ew.write_event(helper.new_event(source=baseurl, index=helper.get_output_index(), sourcetype=helper.get_sourcetype()+':resource:network', data=json.dumps(card)))
                del resource['details']['networkCardDetails']

            #
            # Create resource event for each tenant resource
            #
            ew.write_event(helper.new_event(source=baseurl, index=helper.get_output_index(), sourcetype=helper.get_sourcetype()+':resource', data=json.dumps(resource)))
            
            #
            # Get and cache metrics for each tenant resource
            #
            tenant_resource_metrics[tenant['uniqueId']][resource['id']] = []
            metrics = get_metrics(baseurl, bearer_token, tenant, resource, helper)
            
            #
            # Iterate over tenant resource metrics
            #
            for metric in metrics:
                metric['tenant_id'] = tenant['id']
                metric['tenant_uniqueId'] = tenant['uniqueId']
                metric['tenant_name'] = tenant['name']
                metric['resource_id'] = resource['id']
                metric['resource_name'] = resource['name']
                metric['resource_resourceType'] = resource['resourceType']
                metric['resource_hostName'] = resource['hostName']
                metric['resource_ipAddress'] = resource['ipAddress']
                
                ew.write_event(helper.new_event(source=baseurl, index=helper.get_output_index(), sourcetype=helper.get_sourcetype()+':metric', data=json.dumps(metric)))
                tenant_resource_metrics[tenant['uniqueId']][resource['id']].append(metric['metricName'])
            
    return tenant_resource_metrics
                    
                    
                    
def get_metricdatasets(baseurl, bearer_token, tenant, resource, metric, startTime, endTime, helper):
    url = baseurl + '/api/v2/metric/search?tenant=' + tenant + '&resource=' + resource + '&timeseries_type=RealTime&metric=' + metric + '&startTime=' + str(startTime) + '&endTime=' + str(endTime)
    headers = {
        'Content-Type'      : 'application/json',
        'Accept'            : 'application/json',
        'Authorization'     : 'Bearer ' + bearer_token
    }
    method = "GET"
    response = helper.send_http_request(
        url,
        method, 
        parameters=None, 
        headers=headers, 
        verify=False, 
        timeout=None, 
        use_proxy=has_proxy_configured(helper)
    )
    update_api_stats(response, helper)
    metricdatasets = json.loads(response.text)
    return metricdatasets
                
                
def get_metrics(baseurl, bearer_token, tenant, resource, helper):
    url = baseurl + '/api/v2/metric/tenants/' + tenant['uniqueId'] + '/rtypes/DEVICE/resources/' + resource['id'] + '/metrics'
    headers = {
        'Content-Type'      : 'application/json',
        'Accept'            : 'application/json',
        'Authorization'     : 'Bearer ' + bearer_token
    }
    method = "GET"
    response = helper.send_http_request(
        url,
        method, 
        parameters=None, 
        headers=headers, 
        verify=False, 
        timeout=None, 
        use_proxy=has_proxy_configured(helper)
    )

    metrics = json.loads(response.text)
    return metrics
            
            
def get_resources(baseurl, bearer_token, tenant, helper):

    url = baseurl + '/api/v2/tenants/' + tenant['uniqueId'] + '/resources/minimal'
    headers = {
        'Content-Type'      : 'application/json',
        'Accept'            : 'application/json',
        'Authorization'     : 'Bearer ' + bearer_token
    }
        
    method = "GET"
    
    
    response = helper.send_http_request(
        url,
        method, 
        parameters=None, 
        headers=headers, 
        verify=False, 
        timeout=None, 
        use_proxy=has_proxy_configured(helper)
        )
    update_api_stats(response, helper)
    
    resources = json.loads(response.text)
    return resources
    
    
def get_tenants(baseurl, bearer_token, partner_tenant_id, helper, ew):

    url = baseurl + '/api/v2/tenants/' + partner_tenant_id + '/clients/minimal'
    headers = {
        'Content-Type'      : 'application/json',
        'Accept'            : 'application/json',
        'Authorization'     : 'Bearer ' + bearer_token
    }
        
    method = "GET"
    
    
    response = helper.send_http_request(
        url,
        method, 
        parameters=None, 
        headers=headers, 
        verify=False, 
        timeout=None, 
        use_proxy=has_proxy_configured(helper)
        )
    update_api_stats(response, helper)
    
    tenants = json.loads(response.text)
    return tenants

#
# Given an oauth2 api key and secret, obtain the session bearer token
#
def get_session_token(baseurl, partner_api_key, partner_api_secret, helper):
    
    headers = {
    'Content-Type'      : 'application/x-www-form-urlencoded',
    'Accept'            : 'application/json'
    }
    
    url = baseurl + '/auth/oauth/token'
        
    parameters = {
        'grant_type'    : 'client_credentials',
        'client_id'     : partner_api_key,
        'client_secret' : partner_api_secret
        }
        
    method = "POST"
    
    
    response = helper.send_http_request(
        url,
        method, 
        parameters=parameters, 
        headers=headers, 
        verify=False, 
        timeout=None, 
        use_proxy=has_proxy_configured(helper)
        )
    #response.raise_for_status()

    bodyobj = json.loads(response.text)
    access_token = bodyobj['access_token']
    return access_token
    
    
    
    
    
    
    
    """Implement your data collection logic here

    # The following examples get the arguments of this input.
    # Note, for single instance mod input, args will be returned as a dict.
    # For multi instance mod input, args will be returned as a single value.
    opt_credentials = helper.get_arg('credentials')
    opt_api_endpoint = helper.get_arg('api_endpoint')
    # In single instance mode, to get arguments of a particular input, use
    opt_credentials = helper.get_arg('credentials', stanza_name)
    opt_api_endpoint = helper.get_arg('api_endpoint', stanza_name)

    # get input type
    helper.get_input_type()

    # The following examples get input stanzas.
    # get all detailed input stanzas
    helper.get_input_stanza()
    # get specific input stanza with stanza name
    helper.get_input_stanza(stanza_name)
    # get all stanza names
    helper.get_input_stanza_names()

    # The following examples get options from setup page configuration.
    # get the loglevel from the setup page
    loglevel = helper.get_log_level()
    # get proxy setting configuration
    proxy_settings = helper.get_proxy()
    # get account credentials as dictionary
    account = helper.get_user_credential_by_username("username")
    account = helper.get_user_credential_by_id("account id")
    # get global variable configuration
    global_userdefined_global_var = helper.get_global_setting("userdefined_global_var")

    # The following examples show usage of logging related helper functions.
    # write to the log for this modular input using configured global log level or INFO as default
    helper.log("log message")
    # write to the log using specified log level
    helper.log_debug("log message")
    helper.log_info("log message")
    helper.log_warning("log message")
    helper.log_error("log message")
    helper.log_critical("log message")
    # set the log level for this modular input
    # (log_level can be "debug", "info", "warning", "error" or "critical", case insensitive)
    helper.set_log_level(log_level)

    # The following examples send rest requests to some endpoint.
    response = helper.send_http_request(url, method, parameters=None, payload=None,
                                        headers=None, cookies=None, verify=True, cert=None,
                                        timeout=None, use_proxy=True)
    # get the response headers
    r_headers = response.headers
    # get the response body as text
    r_text = response.text
    # get response body as json. If the body text is not a json string, raise a ValueError
    r_json = response.json()
    # get response cookies
    r_cookies = response.cookies
    # get redirect history
    historical_responses = response.history
    # get response status code
    r_status = response.status_code
    # check the response status, if the status is not sucessful, raise requests.HTTPError
    response.raise_for_status()

    # The following examples show usage of check pointing related helper functions.
    # save checkpoint
    helper.save_check_point(key, state)
    # delete checkpoint
    helper.delete_check_point(key)
    # get checkpoint
    state = helper.get_check_point(key)

    # To create a splunk event
    helper.new_event(data, time=None, host=None, index=None, source=None, sourcetype=None, done=True, unbroken=True)
    """

    '''
    # The following example writes a random number as an event. (Multi Instance Mode)
    # Use this code template by default.
    import random
    data = str(random.randint(0,100))
    event = helper.new_event(source=helper.get_input_type(), index=helper.get_output_index(), sourcetype=helper.get_sourcetype(), data=data)
    ew.write_event(event)
    '''

    '''
    # The following example writes a random number as an event for each input config. (Single Instance Mode)
    # For advanced users, if you want to create single instance mod input, please use this code template.
    # Also, you need to uncomment use_single_instance_mode() above.
    import random
    input_type = helper.get_input_type()
    for stanza_name in helper.get_input_stanza_names():
        data = str(random.randint(0,100))
        event = helper.new_event(source=input_type, index=helper.get_output_index(stanza_name), sourcetype=helper.get_sourcetype(stanza_name), data=data)
        ew.write_event(event)
    '''
