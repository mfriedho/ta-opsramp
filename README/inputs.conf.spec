[opsramp_metrics://<name>]
api_endpoint = i.e. https://companyname.api.opsramp.com
partner_tenant_id = 
partner_api_key = 
partner_api_secret = 
inventory_refresh_frequency = How often (in minutes) the inventory of resources and assigned metrics should be updated.
include_metrics = Comma-separated list of metric name regex patterns to include for data collection (blank means include all)
exclude_metrics = Comma-separated list of metric name regex patterns to exclude for data collection (blank means do not exclude anything)